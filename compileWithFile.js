var FILE_NAME = 
"simple-ex.js"
;

// var FILE_NAME = "";  // uncomment this line if you arent using command line script
// you must also update the file name accordingly to local file.
// optionally if its a function, we should have fields for input params
function symbolTableNode(name) {
	this.children = {};
	this.name = name;
	this.data;
	this.parent;
}

function JSVis() {
	
	this._symbolTableHead = new symbolTableNode("\n_HEAD_");
	this._symbolTableHead.data = "";
	this._symbolTableHead.parent = null;
	// ignore the head from now on; we prob dont even need the head
	// because were now doing bidirectional list
	// just work on the currentNode
	this._symbolTableCurNode = this._symbolTableHead; // cunode is aka scope
	return this;
}

JSVis.prototype = Object.create(JSVis.prototype);
JSVis.prototype.constructor = JSVis;

function printTree (startnode, str) {
	if(!printing) {
		return;
	} 
	str = str || "";
	console.log(str + startnode.name + ": "+ startnode.data);
	for(n in startnode.children) {
		printTree(startnode.children[n], str + "\t");
	}
};


var calledRead = 0;
var printing = false;
var startLines = [];
var endLines = [];
function readIn() {
	var fs = require('fs');
	var contents = fs.readFileSync(FILE_NAME, 'utf8').split("\n");
	// console.log(contents);
	// var count = 0
	for(var i = 0;i < contents.length;i++) {
		var line = contents[i]
			if(line.indexOf("//STARTREC") != -1) {
				startLines.push(i);
			}
			if(line.indexOf("//ENDREC") != -1) {
				endLines.push(i);
			}
    	//code here using lines[i] which will give you each line
	}	

}
readIn();
if (startLines.length == 0) {
	printing = true;
}
// JALANGI CODE
J$.analysis = {};
(function (sandbox){
	function AnalysisEngine(){
		var iidToLocation = sandbox.iidToLocation;
		var jsvis = new JSVis();

		function showLocation(iid){
			var id = J$.getGlobalIID(iid);
			var location = J$.iidToLocation(id);
			// var globID = J$.getGlobalIID(iid);
			console.log("GLOBAL ID IS: " + id);
			console.log("\n... Source Location: " + location);
		};

		function flip(iid){
			var row;
			row = parseInt(J$.iidToLocation(J$.getGlobalIID(iid)).split(":")[1]) - 1;
			if (isNaN(row)) {
				row = parseInt(J$.iidToLocation(J$.getGlobalIID(iid)).split(":")[2]) - 1;
			}
			if (startLines.indexOf( row ) != -1) printing = true;
			if (endLines.indexOf( row ) != -1) printing = false;
		}

		// we actually need the pre invoke to set up the node..
		// this allows you to add stuff to children.

		this.invokeFunPre = function (iid, f, base, args, isConstructor) {
			// make new node for scope of function
			var curNode = new symbolTableNode(f.name);
			// set up doubly linked tree. curnode will be next symboltablecurnode
			curNode.parent = jsvis._symbolTableCurNode;
			curNode._params = args;
			curNode.data = f.name;

			// swap scope. put this node onto children of current scope and swap scope to that node
			jsvis._symbolTableCurNode.children[f] = curNode;
			jsvis._symbolTableCurNode = curNode;

		};

		// do not set up node here
		// rather, change the current scope to jump up.. at the least 1
		this.invokeFun = function(iid, f, base, args, val, isConstructor) {
			if (!printing)
				flip(iid);
			jsvis._symbolTableCurNode = jsvis._symbolTableCurNode.parent;
			if (printing)
				flip(iid)
		};
		// var ISNAN = isNaN;

		this.write = function (iid, name, val, oldValue) {
			if (!printing)
				flip(iid);

			var tempNode = jsvis._symbolTableCurNode.children[name] || new symbolTableNode(name);

			tempNode.parent = jsvis._symbolTableCurNode;
			tempNode.data = val;
			tempNode.name = name;
			jsvis._symbolTableCurNode.children[name] = tempNode;

			printTree(jsvis._symbolTableHead);
			if (printing)
				flip(iid);

		};

		this.read = function (iid, name, val, oldValue) {
			flip(iid);
		};
	}
	sandbox.analysis = new AnalysisEngine();
})(J$);


exports.JSVis = JSVis;
