# README #
This is a repo for a JavaScript execution visualizer.

HW5.1 : https://docs.google.com/document/d/1dj4DZDzfDHKAz8rL3z5QOo41suTW8RCeHbIF2agvVcE/edit

HW5.2 : https://docs.google.com/document/d/1ZDnS9O_00CzOTHeWC3IjiJ-FICcO_14jyDQcoAEqB0g/edit

HW5.3 : https://docs.google.com/document/d/1Rh3LbACPgZhVldldkAAPITPCOjNp2zEepFqHZ-DPrWE/edit

HW5.4 : https://docs.google.com/document/d/1IVeTcH8qRxqdAALnTwSET7jSyWYrb_YlMk3wQ9lODT0/edit?usp=sharing

Slides:
https://docs.google.com/presentation/d/1YuGDySk_F1-sL2Nxk6xEiK_c2ADAfyeiqCBsH9iMdG0/edit?usp=sharing

Screencast:
https://bitbucket.org/thomasnguw/c-visualizer/src/44a000ca5b1255a8d5ac3c9f7d06503b226153be/screencast-viz.mov?fileviewer=file-view-default

### What is this repository for? ###
Automated visualization of execution of JavaScript code.

### How do I get set up? ###
Dependencies:
Relies on jalangi2 and Node.js
You need to install Node.js, jalangi was cloned into the repo already.

1. Clone repo
2. Have your JavaScript file in main directory of repo
3. Run ./vis [YourProgramHere.js]

   3a. By default, the whole program will be visualized

   3b. Otherwise, you can start visualizing all memory changes by putting the
       token "//STARTREC" at the end of a line of code eg
       "var a = 2; //STARTREC"

   3c. Likewise, to stop printing all memory changes, append the token "//ENDREC"
       to a line of code.

Alternatively view screencast for supplementary information.
### Who do I talk to? ###
* Repo owner or admin
Thomas Nguyen - tomn@uw.edu
* Other community or team contact
Shadman Abedin - shadman@uw.edu